FROM python:3.10-alpine

WORKDIR /usr/src/app

RUN apk add --no-cache \
    alpine-conf
RUN pip install --no-cache-dir --upgrade pip

RUN setup-timezone -z Europe/Paris

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
#RUN pip install --no-cache-dir --upgrade --force-reinstall -r requirements.txt

RUN apk del --no-cache \
    alpine-conf


ENV UID=0
ENV MOUNT=/

COPY ./main.py .

ENTRYPOINT ["python3", "main.py"]
